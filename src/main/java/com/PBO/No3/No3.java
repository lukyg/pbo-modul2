package com.PBO.No3;

import java.util.Scanner;

class LimasSegiempat {
    double luas_alas;
    double tinggi;

    double hitungLimas(){
        return 0.3 * tinggi * luas_alas;
    }
}
public class No3 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        LimasSegiempat limasSegiempat = new LimasSegiempat();

        System.out.print("Masukkan luas alas : ");
        limasSegiempat.luas_alas = input.nextDouble();
        System.out.print("Masukkan tinggi : ");
        limasSegiempat.tinggi = input.nextDouble();
        
        System.out.println("Hasil hitung adalah "+limasSegiempat.hitungLimas());
    }
}
