package com.PBO.No9;

class Film{
    String nama;
    String genre;
    String durasi;

    public Film(){
        this.nama = "Titanic";
        this.genre = "Romance";
        this.durasi = "50 menit";
    }

    public Film(String nama){
        this.nama = nama;
    }
    public Film(String nama, String genre){
        this.nama = nama;
        this.genre = genre;
    }
    public Film(String nama, String genre, String durasi){
        this.nama = nama;
        this.genre = genre;
        this.durasi = durasi;
    }
    void display(){
        System.out.println("Nama : "+nama);
        System.out.println("Genre : "+genre);
        System.out.println("Durasi : "+durasi);
        System.out.println("-----------------");
    }
}
public class No9 {
    public static void main(String[] args) {
        Film film1 = new Film();
        Film film2 = new Film("Miracle");
        Film film3 = new Film("Tundra","Horor");
        Film film4 = new Film("Ninja", "Action", "60 menit");

        film1.display();
        film2.display();
        film3.display();
        film4.display();
    }
}
