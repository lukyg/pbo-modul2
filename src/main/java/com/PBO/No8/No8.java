package com.PBO.No8;

import java.util.Scanner;

class IdPass{
    private String username;
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
public class No8 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        IdPass idPass = new IdPass();
        System.out.print("Masukkan username : ");
        idPass.setUsername(input.nextLine());
        System.out.print("Masukkan password : ");
        idPass.setPassword(input.nextLine());
        if(idPass.getUsername().equals("lukyg")&&idPass.getPassword().equals("123")){
            System.out.println("\nBerhasil login");
        }else{
            System.err.println("\nGagal login!");
        }
    }
}
