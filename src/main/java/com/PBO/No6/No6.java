package com.PBO.No6;

import java.util.Scanner;

class SortNilai{

    static String nilaiA(){
        return "Nilai anda A";
    }
    static String nilaiB(){
        return "Nilai anda B";
    }
    static String nilaiC(){
        return "Nilai anda C";
    }
    static String nilaiD(){
        return "Nilai anda D";
    }
    static String nilaiE(){
        return "Nilai anda E";
    }
}
public class No6 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan nilai : ");
        double nilai = input.nextDouble();
        if(nilai >= 80 && nilai <= 100){
            System.out.println(SortNilai.nilaiA());
        }else if(nilai >= 72.5 && nilai < 80){
            System.out.println(SortNilai.nilaiB());
        }else if(nilai >= 65 && nilai <72.5){
            System.out.println(SortNilai.nilaiC());
        }else if(nilai >= 55 && nilai < 65){
            System.out.println(SortNilai.nilaiD());
        }else if(nilai >= 0 && nilai < 55){
            System.out.println(SortNilai.nilaiE());
        }else{
            System.out.println("Input nilai yang benar");
        }
    }
}
