package com.PBO.No4;

import java.util.Scanner;

class Identitas{
    String nama;
    String alamat;
    String hobi;
    String makanan;
}
public class No4{
    public static void main(String[] args) {
        Identitas identitas = new Identitas();
        Scanner input = new Scanner(System.in);
        System.out.print("Nama : ");
        identitas.nama = input.nextLine();
        System.out.print("Alamat : ");
        identitas.alamat = input.nextLine();
        System.out.print("Hobi : ");
        identitas.hobi = input.nextLine();
        System.out.print("Makanan : ");
        identitas.makanan = input.nextLine();
        input.close();

        System.out.println("\n---------- Hasil manipulasi-----------");
        System.out.println("Nama : "+identitas.nama.toUpperCase());
        System.out.println("Alamat : "+identitas.alamat.toUpperCase());
        System.out.println("Hobi : "+identitas.hobi.toUpperCase());
        System.out.println("Makanan : "+identitas.makanan.toUpperCase());
    }
}
