package com.PBO.No5;

import java.util.Scanner;

class HitungLimas {
    double luasAlas;
    double tinggi;

    double volLimas (double luasAlas, double tinggi){
        return 0.3 * luasAlas * tinggi;
    }
}
public class No5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        HitungLimas nilai = new HitungLimas();
        System.out.print("Masukkan luas alas : ");
        nilai.luasAlas = input.nextDouble();
        System.out.print("Masukkan tinggi : ");
        nilai.tinggi = input.nextDouble();

        System.out.println("Hasil hitung adalah "+ nilai.volLimas(nilai.luasAlas, nilai.tinggi));
    }
}
